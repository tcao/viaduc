import sys
import logging
from tweets import mention_extractor
from flask import (Flask, flash, g, jsonify, redirect, render_template,
                   request, url_for, Response)


LOG_FILE = 'dev.log' if '--dev' in sys.argv else 'politique1.log'
PRODUCTION_PORT = 5000
DEVELOPMENT_PORT = 6000

app = Flask(__name__)

handler = logging.FileHandler(LOG_FILE)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
app.logger.addHandler(handler)
logger = app.logger
if '--dev' in sys.argv:
    logger.setLevel(level=logging.DEBUG)
else:
    logger.setLevel(level=logging.INFO)

@app.route('/')
def list_mentions():
    dates = ['2019-07-10']
    return render_template('index.html', dates=dates)

def stream_template(template_name, **context):
    app.update_template_context(context)
    t = app.jinja_env.get_template(template_name)
    rv = t.stream(context)
    # rv.enable_buffering(5)
    return rv

@app.route('/tweets/<published_date>')
def tweets_of_mention(published_date):
    return Response(stream_template('mentions.html', \
            tweets=mention_extractor.extract_from_candidate(published_date)))

if __name__ == "__main__":
    dev_mode = '--dev' in sys.argv
    app.run(port=DEVELOPMENT_PORT if dev_mode else PRODUCTION_PORT)
