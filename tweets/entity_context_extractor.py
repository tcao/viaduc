import os
import math
import re
import spacy
import sys


nlp = spacy.load('fr_core_news_md')


with open('resources/insee_verbs.txt') as f:
    verbs = f.read().splitlines()

units = {'%',
 'auto-entrepreneurs',
 'bébés',
 'bénéficiaires',
 'chiffres',
 'chômeurs',
 'croissance',
 'créations',
 'demandes',
 'demandeurs',
 'destructions',
 'dollars',
 'défaillances',
 'emplois',
 'entreprises',
 'etp',
 'euros',
#  'fois',
 'fonctionnaires',
 'habitants',
#  'heures',
 'immatriculations',
#  'jours',
 'kilomètres',
 'milliard',
 'milliards',
 'million',
 'millions',
 'ménages',
 'parents',
 'pauvreté',
 'personnes',
 'pertes',
 'PIB',
 'point',
 'points',
 'postes',
 'salariés',
#  'semaines',
 'suppressions',
 'unités',
  'inscrits'
}
def correct_numbers(sentence):
    new_sentence = sentence
    for match in re.finditer('\d+(\s\d+)+', sentence):
        original = match.group()
        new_sentence = new_sentence.replace(original, ''.join(original.split(' ')))
    for match in re.finditer('\d+(\.\d+)+', sentence):
        original = match.group()
        new_sentence = new_sentence.replace(original, ''.join(original.split('.')))
    return new_sentence

def is_year(num):
    try:
        return 1900 <= int(num) and int(num) <= 3000
    except:
        return False

def collect_sentences(keyword, sentences):
    results = list()
    for sentence in sentences:
        if keyword in sentence:
            results.append(sentence)
    return results


import nltk
from nltk.corpus import stopwords
nltk.download('stopwords')
french_stop_words = stopwords.words('french')
french_stop_words.extend(['à', 'sous', '–', '-', 'les'])


def remove_stop_words(text):
    result = ''
    for token in nlp(text):
        if token.text not in french_stop_words:
            result += token.text + ' '
    return result.strip()

def normalize_extraction(extraction):
#     extraction = extraction\
#         .replace("d' ", "d'")\
#         .replace("n' ", "n'")\
#         .replace(" , ", " ")
    if extraction[-1] in ['.', ',']:
        extraction = extraction[:-1]
    return extraction


# In[ ]:


def list_relationship(doc):
    relationships = dict()
    for token in doc:
        relationships[(token, token.head)] = token.dep_
        relationships[(token.head, token)] = 'reversed_' + token.dep_
    return relationships

def list_neighbors(doc):
    neighbors = dict()
    for token in doc:
        if token not in neighbors:
            neighbors[token] = set()
        if token.head not in neighbors:
            neighbors[token.head] = set()
        neighbors[token].add(token.head)
        neighbors[token.head].add(token)
        
    return neighbors

def bfs_paths(graph, start, goal):
    queue = [(start, [start])]
    while queue:
        (vertex, path) = queue.pop(0)
        for next in graph[vertex] - set(path):
            if next == goal:
                yield path + [next]
            else:
                queue.append((next, path + [next]))

def find_connections_in_doc(doc, start_node, end_node):
    neighbors = list_neighbors(doc)
    connections = list(bfs_paths(neighbors, start_node, end_node))
    if connections:
        connections = connections[0]
        relationships = list_relationship(doc)

        paths = list()
        for i in range(len(connections)):
            if i + 1 < len(connections):
                paths.append((connections[i].pos_, relationships[(connections[i], connections[i + 1])], connections[i + 1].pos_))

        yield paths, connections
    return None

def expand_verb(node):
    results = list()
    for child in node.children:
#         if child.dep_ in ['obl', 'iobj']:
        if child.dep_ in ['obl']:
            subtree = list(child.subtree)
            has_number = len([node for node in subtree if node.pos_ == 'NUM']) > 0
            start_with_prep = subtree[0].text in ['en', 'à', 'dans']
            if not has_number and start_with_prep:
                results.extend(subtree)
    return results

def expand_node3(node, selected_nodes):
    results = [node]
    for child in node.children:
        if child.dep_ in ['nmod', 'amod']:
            results.extend(c for c in child.subtree if c not in selected_nodes)
    return results


def normalize_text3(sentence):
    doc = nlp(correct_numbers(sentence.replace("’", "'")))
    new_sentence = list()
    new_sentence = list()
    for i, token in enumerate(doc):
        if token.text.lower() == 'des' and i + 1 < len(doc):
            if doc[i + 1].pos_ in ['NOUN', 'NUM'] or doc[i + 1].text == 'plus':
                new_sentence.extend(['de', 'les'])
            else:
                new_sentence.append(token.text)
        elif "'" in token.text and token.text != "s'":
            new_sentence.append(token.lemma_)    
        else:
            new_sentence.append(token.text)    
    return ' '.join(new_sentence).replace("s' ", "s'")

def nodes_to_text(nodes):
    return [node.lemma_ for node in nodes]

def find_term(doc, search_term):
    target = nodes_to_text(nlp(search_term))
    for i, node in enumerate(doc):
        if nodes_to_text(doc[i : i + len(target)]) == target:
            return doc[i : i + len(target)]

def extract_entity_and_context(sentence, search_term):
    doc = nlp(normalize_text3(sentence))
    
    term = find_term(doc, search_term)
    search_term_head, search_term_tail = term[0], term[-1]
    selected_pattern, selected_nodes = None, None
    unit = None
    min_abs_distance = 1e9
    for num in [token for token in doc if token.pos_ == 'NUM' and not is_year(token.text)]:
        for pattern, nodes in find_connections_in_doc(doc, search_term_head, num):
            has_valid_unit = False
            if len(pattern) >= 2 and pattern[-1] == ('NOUN', 'reversed_nummod', 'NUM'):
                potential_noun = nodes[-2]
                if potential_noun.pos_ == 'NOUN' and potential_noun.text in units:
                    has_valid_unit = True
                    unit = potential_noun
            if has_valid_unit:
                abs_distance = abs(min(nodes[-1].i - nodes[0].i, search_term_tail.i - nodes[-1].i))
                if abs_distance < min_abs_distance:
                    selected_pattern = pattern
                    selected_nodes = nodes
                    min_abs_distance = abs_distance
        
    if selected_pattern and selected_nodes:
        if len(pattern) >= 2 and pattern[-1] == ('NOUN', 'reversed_nummod', 'NUM'):
            potential_noun = nodes[-2]
            if potential_noun.pos_ == 'NOUN' and potential_noun.text in units:
                if potential_noun.i < nodes[0].i:
                    pos_sequences = [token.pos_ for token in doc[potential_noun.i + 1: nodes[0].i]]
                    if pos_sequences in [['DET'], ['ADP']]:
                        return list(), list(), list()

        output = expand_node3(selected_nodes[0], selected_nodes)
                            
        # only 1 verb
        has_one_verb = sum(1 for node in selected_nodes if node.pos_ == 'VERB') == 1
        has_nsubj_edge = selected_pattern[0] == ('NOUN', 'nsubj', 'VERB')
        if has_one_verb:
            for node in selected_nodes:
                if node.pos_ == 'VERB' and node.lemma_ in verbs:
                    for token in expand_verb(node):
                        if token not in output and token not in selected_nodes:
                            output.append(token)
        
        locations = [ent for ent in doc.ents if ent.label_ == 'LOC']
        output.extend(locations)
        
        return output, selected_nodes, selected_pattern
    return list(), list(), list()


if __name__ == '__main__':
    print(extract_entity_and_context(sys.argv[1], sys.argv[2]))
