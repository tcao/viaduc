import pickle
from tweets.entity_context_extractor import extract_entity_and_context, nlp
from tweets.date_time_extractor import extract_datetime


all_terms = set(open('resources/unit_entities.txt').read().splitlines())


def __highlighted(text, color):
    return f'<mark style="background-color: {color}">{text}</mark>'


def html_from(nodes, context):
    doc = nodes[0].doc
    context_node_ids = list()
    for node in context[1:]:
        if hasattr(node, 'i'):
            context_node_ids.append(node.i)
        else:
            context_node_ids.extend(list(range(node.start, node.end)))

    if nodes[-1].pos_ == 'NUM':
        html_texts = list()
        for node in doc:
            if node.i == nodes[0].i:
                html_texts.append(__highlighted(node.text, 'green'))
            elif node.i == nodes[-1].i or node.i == nodes[-2].i:
                html_texts.append(__highlighted(node.text, 'red'))
            elif node.i in context_node_ids:
                html_texts.append(__highlighted(node.text, 'cyan'))
            else:
                html_texts.append(node.text)

        return ' '.join(html_texts)

    return doc.text


def extract_from_tweet(tweet, published_date, term):
    results = list()
    for sentence_ in nlp(tweet).sents:
        sentence = sentence_.text
        if term not in sentence:
            continue
        try:
            context, nodes, _ = extract_entity_and_context(sentence, term)
            if nodes:
                dates = extract_datetime(sentence, published_date)
                results.append({
                        'published_date': published_date,
                        'tweet': tweet,
                        'dates': dates,
                        'html': html_from(nodes, context)
                })
        except Exception as e:
            print(e)

    return results


def extract_from_candidate(_published_date):
    with open('candidate_tweets.pk', 'rb') as _f:
        candidates = pickle.load(_f)

    counter = 0
    matched = 0
    for tweet, published_date in candidates:
        if published_date == _published_date:
            lowered_tweet = tweet.lower()
            for term in all_terms:
                if term in lowered_tweet:
                    matched += 1
                    results = extract_from_tweet(tweet, published_date, term.split(' ')[0])
                    if results:
                        for data in results:
                            counter += 1
                            yield {'data': data, 'progress': f'{counter}/{matched}'}
