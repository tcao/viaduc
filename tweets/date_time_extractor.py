import os
import subprocess
import xml.etree.ElementTree as et

def extract_datetime(sentence, published_date):
    input_file = 'heidel_in.txt'
    with open(input_file, 'w+') as f:
        f.write(sentence)
    
    temp_file = f'heidel_temp.txt'
    with open(temp_file, 'w+') as f:
        f.write('')
        
    datetimes = list()
    try:
        subprocess.call('''java -jar de.unihd.dbs.heideltime.standalone.jar %s -l FRENCH \
                -t NEWS -pos TREETAGGER -dct %s > %s''' % (input_file, published_date, temp_file), shell=True)

        tree = et.parse(temp_file)
        root = tree.getroot()
        for child in root:
            datetimes.append((child.attrib['type'], child.attrib['value']))
    except Exception as ex:
        print(ex)
        return None

    os.remove(temp_file)
    os.remove(input_file)

    if not datetimes or (len(datetimes) == 1 and 'REF' in datetimes[0][1]):
        return [('DATE', published_date[:4])]
    return datetimes


if __name__ == '__main__':
    import sys
    print(extract_datetime(sys.argv[1], sys.argv[2]))
