import preprocessor as p
import re


PATTERN_LOWER_CASE = re.compile('[a-z]+\Z')
PATTERN_UPPER_CASE = re.compile('[A-Z]+\Z')
PATTERN_MULTIPLE_WORDS = re.compile('(\w+)?([A-Z][a-z]?)+')
PATTERN_NUMBER = '(^|\s)(\-?\d+(?:(\.|\,)\d)*|\d+)(^|\s|\.|%|\,)'

special_components = ['hashtags', 'urls', 'smileys', 'emojis', 'mentions', 'reserved_words']
# Twitter screen name -> user name
dict_user_names = {screen_name: user_name for screen_name, user_name in \
        [line.split(',') for line in open('politique1_user_names.csv').read().splitlines()]}

def clean_hashtag(hashtag):
    hashtag = hashtag[1:]

    if PATTERN_UPPER_CASE.match(hashtag) or PATTERN_LOWER_CASE.match(hashtag):
        return hashtag

    if PATTERN_MULTIPLE_WORDS.match(hashtag):
        new_characters = list()
        for character in hashtag:
            new_characters.append(' ' + character if character.isupper() else character)

        new_text = ''.join(new_characters).strip()
        if new_text[0].islower():
            return new_text.lower()
        else:
            return new_text

    return None


def clean_mention(mention):
    if mention[1:] in dict_user_names:
        return dict_user_names[mention[1:]]
    return clean_hashtag(mention.replace('_', ''))


def get_candidate(tweet):
    tweet_content, published_date = tweet
    parsed_tweet = p.parse(tweet_content)
    match = re.search(PATTERN_NUMBER, tweet_content)
    if match:
        for component in special_components:
            attribute = getattr(parsed_tweet, component)
            if attribute:
                for text in attribute:
                    if component in ['emojis', 'smileys', 'urls']:
                        tweet_content = tweet_content.replace(text.match, ' ')
                    if component == 'hashtags':
                        tweet_content = tweet_content.replace(text.match, clean_hashtag(text.match) or '')
                    if component == 'mentions':
                        tweet_content = tweet_content.replace(text.match, clean_mention(text.match) or '')
        return tweet_content, published_date, parsed_tweet

    return None, None, None
