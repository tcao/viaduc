import json
import os


def tweet_from_json(_json):
    dictionary = json.loads(_json)
    return dictionary['tweet'], dictionary['date']


def tweets_from_file(json_file):
    with open(json_file) as f:
        for _json in f.read().splitlines():
            try:
                yield tweet_from_json(_json)
            except:
                pass


def tweets_from_date(_date, tweets_json_folder='/data/tcao/list_politique1/'):
    for json_file in os.listdir(tweets_json_folder):
        for tweet, published_date in tweets_from_file(tweets_json_folder + json_file):
            if published_date == _date:
                yield tweet


if __name__ == '__main__':
    tweet = tweet_from_json('''{"id": 1149006243241037824, "conversation_id": "1149006243241037824", "created_at": 1562779410000, "date": "2019-07-10", "time": "19:23:30", "timezone": "CEST", "user_id": 866717602298691584, "username": "zivkapark", "name": "Zivka Park", "place": null, "tweet": "Déception quant à l'issue de cette CMP: un beau travail collectif gâché et l'intérêt de nos concitoyens sacrifié pour des guéguerres politiques LR. #loimobilités @LaREM_AN @DamienPichereau @AN_DevDur @barbarapompili @BrunoMillienne @Jean_LucFUGIT @b_abba @BCouillard33 @jmzulesi pic.twitter.com/e4OLt5ICe2", "mentions": ["larem_an", "damienpichereau", "an_devdur", "barbarapompili", "brunomillienne", "jean_lucfugit", "b_abba", "bcouillard33", "jmzulesi"], "urls": [], "photos": ["https://pbs.twimg.com/media/D_IXEo2WwAEAQ1U.jpg", "https://pbs.twimg.com/media/D_IXFQcXUAA2_Mf.jpg"], "replies_count": 0, "retweets_count": 13, "likes_count": 24, "location": "", "hashtags": ["#loimobilités"], "link": "https://twitter.com/zivkapark/status/1149006243241037824", "retweet": null, "quote_url": "", "video": 0}''')
    print(tweet)
